��      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Installation�h]�h	�Text����Installation�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�4C:\Users\adura\Documents\pygcc\docs\installation.rst�hKubh	�	paragraph���)��}�(h�`pygcc is available on `PyPi <https://pypi.org/project/pygcc/>`_, and can be downloaded with pip:�h]�(h�pygcc is available on �����}�(h�pygcc is available on �hh/hhhNhNubh	�	reference���)��}�(h�)`PyPi <https://pypi.org/project/pygcc/>`_�h]�h�PyPi�����}�(h�PyPi�hh:hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�hB�refuri��https://pypi.org/project/pygcc/�uh+h8hh/ubh	�target���)��}�(h�" <https://pypi.org/project/pygcc/>�h]�h}�(h!]��pypi�ah#]�h%]��pypi�ah']�h)]��refuri�hKuh+hL�
referenced�Khh/ubh�!, and can be downloaded with pip:�����}�(h�!, and can be downloaded with pip:�hh/hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�literal_block���)��}�(h�pip install --user pygcc�h]�h�pip install --user pygcc�����}�(hhhhiubah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��force���language��bash��highlight_args�}�uh+hghh,hKhhhhubh	�note���)��}�(hX�  With pip, a user-level installation is generally recommended (i.e. using the
:code:`--user` flag), especially on systems which have a system-level
installation of Python (including `*.nix`, WSL and MacOS systems); this can
avoid some permissions issues and other conflicts. For detailed information
on other pip options, see the relevant
`docs <https://pip.pypa.io/en/stable/user_guide>`__.
pygcc is not yet packaged for Anaconda, and as such
:code:`conda install pygcc` will not work.�h]�h.)��}�(hX�  With pip, a user-level installation is generally recommended (i.e. using the
:code:`--user` flag), especially on systems which have a system-level
installation of Python (including `*.nix`, WSL and MacOS systems); this can
avoid some permissions issues and other conflicts. For detailed information
on other pip options, see the relevant
`docs <https://pip.pypa.io/en/stable/user_guide>`__.
pygcc is not yet packaged for Anaconda, and as such
:code:`conda install pygcc` will not work.�h]�(h�MWith pip, a user-level installation is generally recommended (i.e. using the
�����}�(h�MWith pip, a user-level installation is generally recommended (i.e. using the
�hh�hhhNhNubh	�literal���)��}�(h�:code:`--user`�h]�h�--user�����}�(h�--user�hh�hhhNhNubah}�(h!]�h#]��code�ah%]�h']�h)]�uh+h�hh�ubh�Z flag), especially on systems which have a system-level
installation of Python (including �����}�(h�Z flag), especially on systems which have a system-level
installation of Python (including �hh�hhhNhNubh	�title_reference���)��}�(h�`*.nix`�h]�h�*.nix�����}�(hhhh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubh��, WSL and MacOS systems); this can
avoid some permissions issues and other conflicts. For detailed information
on other pip options, see the relevant
�����}�(h��, WSL and MacOS systems); this can
avoid some permissions issues and other conflicts. For detailed information
on other pip options, see the relevant
�hh�hhhNhNubh9)��}�(h�3`docs <https://pip.pypa.io/en/stable/user_guide>`__�h]�h�docs�����}�(h�docs�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�h�hJ�(https://pip.pypa.io/en/stable/user_guide�uh+h8hh�ubh�6.
pygcc is not yet packaged for Anaconda, and as such
�����}�(h�6.
pygcc is not yet packaged for Anaconda, and as such
�hh�hhhNhNubh�)��}�(h�:code:`conda install pygcc`�h]�h�conda install pygcc�����}�(h�conda install pygcc�hh�hhhNhNubah}�(h!]�h#]�h�ah%]�h']�h)]�uh+h�hh�ubh� will not work.�����}�(h� will not work.�hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h~hhhhhh,hNubh)��}�(hhh]�(h)��}�(h�Upgrading pygcc�h]�h�Upgrading pygcc�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh.)��}�(h��New versions of pygcc are released frequently. You can upgrade to the latest edition
on `PyPi <https://pypi.org/project/pygcc/>`_ using the :code:`--upgrade` flag:�h]�(h�XNew versions of pygcc are released frequently. You can upgrade to the latest edition
on �����}�(h�XNew versions of pygcc are released frequently. You can upgrade to the latest edition
on �hj   hhhNhNubh9)��}�(h�)`PyPi <https://pypi.org/project/pygcc/>`_�h]�h�PyPi�����}�(h�PyPi�hj	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�j  hJ�https://pypi.org/project/pygcc/�uh+h8hj   ubhM)��}�(h�" <https://pypi.org/project/pygcc/>�h]�h}�(h!]��id1�ah#]�h%]�h']��pypi�ah)]��refuri�j  uh+hLh[Khj   ubh� using the �����}�(h� using the �hj   hhhNhNubh�)��}�(h�:code:`--upgrade`�h]�h�	--upgrade�����}�(h�	--upgrade�hj,  hhhNhNubah}�(h!]�h#]�h�ah%]�h']�h)]�uh+h�hj   ubh� flag:�����}�(h� flag:�hj   hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�hhubhh)��}�(h�pip install --upgrade pygcc�h]�h�pip install --upgrade pygcc�����}�(hhhjF  ubah}�(h!]�h#]�h%]�h']�h)]�hwhxhy�hz�bash�h|}�uh+hghh,hKhh�hhubeh}�(h!]��upgrading-pygcc�ah#]�h%]��upgrading pygcc�ah']�h)]�uh+h
hhhhhh,hKubeh}�(h!]��installation�ah#]�h%]��installation�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jc  j`  hWhTj[  jX  u�	nametypes�}�(jc  NhW�j[  Nuh!}�(j`  hhThNjX  h�j   j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  Ks��R��parse_messages�]�h	�system_message���)��}�(hhh]�h.)��}�(h�'Duplicate explicit target name: "pypi".�h]�h�+Duplicate explicit target name: “pypi”.�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hj�  ubah}�(h!]�h#]�h%]�h']�h)]�j   a�level�K�type��INFO��source�h,�line�Kuh+j�  hh�hhhh,hKuba�transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.
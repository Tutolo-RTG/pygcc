# Contributing

The long-term aim of this project is to be designed, built and supported by (and for) the geochemistry community. 
Contributions are welcome in any form - feature requests, bug reports and contributions to the code base, documentation and test suite, and they are greatly appreciated! Every little bit
helps, and credit will always be given.


## Types of Contributions

### Report Bugs

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

### Fix Bugs

Look through the [Changelog page](https://pygcc.readthedocs.io/en/latest/changelog.html) for fixes or [see the relevant contact page](https://pygcc.readthedocs.io/en/latest/pygcc_overview.html#citation-and-contact-information-a-class-anchor-id-section-6-a).

### Write Documentation

You can never have enough documentation! Please feel free to contribute to any
part of the documentation, such as the official docs,  or docstrings.

### Submit Feedback

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

## Get Started!

Ready to contribute? Here's how to set up `pygcc` for local development.

1. Download a copy of `pygcc` locally.
2. Install `pygcc` using `poetry`:

    ```console
    $ poetry install
    ```

3. Use `git` (or similar) to create a branch for local development and make your changes:

    ```console
    $ git checkout -b name-of-your-bugfix-or-feature
    ```

4. When you're done making changes, check that your changes conform to any code formatting requirements and pass any tests.

5. Commit your changes and open a pull request.

## Pull Request Guidelines

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should include additional tests if appropriate.
2. If the pull request adds functionality, the docs should be updated.
3. The pull request should work for all currently supported operating systems and versions of Python.

## Code of Conduct

Please note that the `pygcc` project is released with a 
Code of Conduct. By contributing to this project you agree to abide by its terms.
